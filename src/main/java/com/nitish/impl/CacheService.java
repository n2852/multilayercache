package com.nitish.impl;

import com.nitish.apis.ICacheLayer;
import com.nitish.apis.ICacheService;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CacheService<K,V> implements ICacheService<K, V> {

    List<ICacheLayer> cacheLayers;
    int layerSize = -1;
    int capacityOfEachLayer = -1;
    int totalElementAdded = 0;

    public CacheService(int layerSize, int capacityOfEachLayer) {
        initOrResetCache(layerSize, capacityOfEachLayer);
    }

    @Override
    public void initOrResetCache(int layerSize, int capacityOfEachLayer) {
        cacheLayers = new ArrayList<>();
        for(int i=0; i<layerSize; i++) {
            cacheLayers.add(new CacheLayer(capacityOfEachLayer));
        }
        this.layerSize = layerSize;
        this.capacityOfEachLayer = capacityOfEachLayer;
        totalElementAdded = 0;
    }

    @Override
    public Optional<V> readFromCache(K key) {
        System.out.println();
        System.out.println("-------------------------------READ OPERATION START------------------------------------------------------------");
        System.out.println("Reading from the Cache for the key: " + key);


        Optional<V> value = Optional.empty();
        int i;
        for(i=0; i<layerSize && !value.isPresent(); i++) {
            System.out.println("Reading key " + key + " from cache layer: " + i);
            value = cacheLayers.get(i).readFromCache(key);
            System.out.println("Response from cache layer " + value + " value");
        }

        if(i > 1 && value.isPresent()) {
            removeKey(i-1 ,key);
            System.out.println("Pushing key-value pair " + key +", " + value + " to the top most layer" );
            writeToCache(key, value.get());
        }

        System.out.println("Returning " + value + " from the Cache");
        System.out.println("-------------------------------READ OPERATION END------------------------------------------------------------");

        return value;
    }

    @Override
    public boolean writeToCache(K key, V value) {
        System.out.println();
        System.out.println("-------------------------------WRITE OPERATION START------------------------------------------------------------");
        System.out.println("Trying to write key: " + key + ", value: " + value + " to the Cache");

        if(totalElementAdded == layerSize*capacityOfEachLayer) {
            System.out.println("ERROR: Cache is full.... Element can not be added");
            return false;
        }

        Pair<K, V> keyValueToWrite = new Pair<K,V>(key, value);
        for(int i=0; i<layerSize && keyValueToWrite != null; i++) {
            System.out.println("Trying to write " + keyValueToWrite + ", to layer " + i);

            keyValueToWrite = cacheLayers.get(i).writeToCache(keyValueToWrite.getKey(), keyValueToWrite.getValue());

            System.out.println("Successfully wrote the pair in the " + i +" layer");
            System.out.println("Received key-value pair " + keyValueToWrite + " to write onto next layer " + (i+1));
        }
        totalElementAdded++;
        System.out.println("Total element in the cache: " + totalElementAdded);
        System.out.println("-------------------------------WRITE OPERATION END------------------------------------------------------------");
        return true;
    }

    @Override
    public void removeKey(int i, K key) {
        cacheLayers.get(i).removeKey(key);
        totalElementAdded--;
    }
}
