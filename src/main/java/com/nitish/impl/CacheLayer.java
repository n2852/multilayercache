package com.nitish.impl;

import com.nitish.apis.ICacheLayer;
import javafx.util.Pair;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class CacheLayer<K, V> implements ICacheLayer<K, V> {
    Map<K, V> cacheElementMap = new HashMap<>();
    int capacity;

    CacheLayer(int capacity) {
        if(capacity == 0) {
            throw new IllegalArgumentException("Capacity cannot be zero");
        }
        this.capacity = capacity;
    }

    @Override
    public Optional<V> readFromCache(K key) {
        if(cacheElementMap.isEmpty() || !cacheElementMap.containsKey(key)) {
            return Optional.empty();
        }
        return Optional.of(cacheElementMap.get(key));
    }

    @Override
    public Pair<K, V> writeToCache(K key, V value) {
        Pair<K, V> pair = null;
        System.out.println("Current size of the layer: " + cacheElementMap.size());
        if(cacheElementMap.size() == capacity) {
            Optional<K> anyRandomKey = cacheElementMap.keySet().stream().findAny();
            K randomKey = anyRandomKey.get();
            pair = new Pair<K, V>(randomKey, cacheElementMap.remove(randomKey));
        }

        cacheElementMap.put(key, value);
        return pair;
    }

    @Override
    public void removeKey(K key) {
        cacheElementMap.remove(key);
    }
}
