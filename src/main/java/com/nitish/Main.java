package com.nitish;

import com.nitish.apis.ICacheService;
import com.nitish.impl.CacheService;

import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        ICacheService<Integer, String> cacheService = new CacheService<>(2, 3);
        String[] strings = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j"};

        for(int i=0; i<10; i++) {
            boolean isSuccess = cacheService.writeToCache(i, strings[i]);
            System.out.println("WRITE OPERATION " + (isSuccess ? "SUCESS!!!" : "FAILED!!!"));
        }

        for(int i=0; i<10; i++) {
            Optional<String> value = cacheService.readFromCache(i);
            System.out.println(value.isPresent() ? "VALUE RETURNED: " + value.get() : "NO VALUE FOUND");
        }
    }
}
