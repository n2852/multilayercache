package com.nitish.apis;

import javafx.util.Pair;

import java.util.Optional;

public interface ICacheLayer<K, V> {

    Optional<V> readFromCache(K key);
    Pair<K, V> writeToCache(K key, V value);

    void removeKey(K key);
}
