package com.nitish.apis;

import java.util.Optional;

public interface ICacheService<K, V> {

    public void initOrResetCache(int layerLength, int capacityForEachLayer);

    public Optional<V> readFromCache(K key);

    public boolean writeToCache(K key, V value);

    void removeKey(int i, K key);
}
